package com.smartstock.ia.utils;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;

public class DataPreview {

	
	public void dataLoad() {
		
		SparkSession spark = SparkSession.builder().master("local").appName("smart-stock-ia").getOrCreate();
		//enables cross	joining across Spark DataFrames
		spark.conf().set("spark.sql.crossJoin.enabled", true);
		
		// load data from csv file
		String fileName = "price-stock.csv";
		//String fileName = "bitstampUSD_1-min_data_2012-01-01_to_2020-12-31.csv";
		Dataset<Row> data = spark.read().option("inferSchema", false)
										.option("header", true)
										.option("delimiter", ";")
										.format("csv")
										.load(fileName)
										.withColumn("open", functions.col("openPrice").cast("double"))
										.drop("openPrice")
										.withColumn("close",functions.col("closePrice").cast("double"))
										.drop("closePrice")
										.withColumn("low", functions.col("lowPrice").cast("double"))
										.drop("lowPrice")
										.withColumn("high", functions.col("highPrice").cast("double"))
										.drop("highPrice")
										.withColumn("volume", functions.col("volumeTmp").cast("double"))
										.drop("volumeTmp")
										.toDF("date", "symbol", "open", "close", "low", "high", "volume");
		data.show(10);
		
		data.createOrReplaceTempView("stock");
		spark.sql("SELECT DISTINCT symbol FROM stock GROUP BY symbol").show(10);
		
		spark.sql("SELECT symbol, avg(open) as avg_open, 	"
				+ "		  avg(close) as avg_close,			"
				+ "		  avg(low) as avg_low,				"
				+ "		  avg(high) as avg_high 			"
				+ "		FROM stock GROUP BY symbol 			").show(10);

		spark.sql("SELECT symbol, 				"
				+ "	MIN(open) as min_open,		"
				+ "	MAX(open) as max_open,		"
				+ "	MIN(close) as min_close,	"
				+ "	MAX(low) as max_low,		"
				+ "	MIN(low) as min_low,		"
				+ "	MIN(high) as min_high,		"
				+ "	MAX(high) as max_high		"
				+ "	FROM stock GROUP BY symbol	").show(10);
		
        long count = data.select("symbol").count();
        System.out.println("Number of Symbols: " + count);
		
	}
	
}
