package com.smartstock.ia.representation;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
public @Data class StockData {
	private String date;
	private String symbol;
	private Double open;
	private Double close;
	private Double low;
	private Double high;
	private Double volume;
}
