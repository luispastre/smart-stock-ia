package com.smartstock.ia.representation;

public enum PriceCategory {
	OPEN, CLOSE, LOW, HIGH, VOLUME, ALL;
}
