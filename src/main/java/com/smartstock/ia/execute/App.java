package com.smartstock.ia.execute;

import java.io.File;
import java.util.List;
import java.util.NoSuchElementException;

import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.eval.RegressionEvaluation;
import org.deeplearning4j.nn.api.Layer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.primitives.Pair;

import com.smartstock.ia.db.bo.StockBO;
import com.smartstock.ia.prediction.RecurrentNets;
import com.smartstock.ia.prediction.RecurrentNetsHGLG11;
import com.smartstock.ia.prediction.RecurrentNetsPETR4;
import com.smartstock.ia.prediction.RecurrentNetsSQIA3;
import com.smartstock.ia.prediction.RecurrentNetsVALE3;
import com.smartstock.ia.prediction.RecurrentNetsVISC11;
import com.smartstock.ia.representation.PriceCategory;
import com.smartstock.ia.representation.StockDataSetIterator;
import com.smartstock.ia.utils.PlotUtil;
import com.smartstock.ia.utils.StockEnum;

public class App {
	//private static String symbol = StockEnum.SQIA3.name(); //stock name
	private static int exampleLength = 22; // time series length, assume 22 working days per month
	private static StockDataSetIterator iterator;
	private static String fileName = "data/price-stock.csv";
	private static int batchSize = 128; //mini-batch size
	private static double splitRatio = 0.8; //80% for training, 20% for testing
	 
	
	
    public static void main( String[] args ) throws Exception{
    	//String symbol = StockEnum.SQIA3.name(); //stock name
    	//int batchSize = 128; //mini-batch size
    	//double splitRatio = 0.8; //80% for training, 20% for testing
    	//int epochs = 120; //training epochs
    	
    	predict(StockEnum.SQIA3.name(), 120);
    	predict(StockEnum.PETR4.name(), 80);
    	predict(StockEnum.VALE3.name(), 90);
    	predict(StockEnum.HGLG11.name(), 100);
    	predict(StockEnum.VISC11.name(), 100);
    	
    }
    
    private static void predict(String symbol, int epochs) throws Exception {
    	
    	startPredict(symbol, epochs, PriceCategory.OPEN);
    	startPredict(symbol, epochs, PriceCategory.CLOSE);
    	startPredict(symbol, epochs, PriceCategory.HIGH);
    	startPredict(symbol, epochs, PriceCategory.LOW);
    	startPredict(symbol, epochs, PriceCategory.VOLUME);
    	
    	//startPredict(symbol, epochs, PriceCategory.ALL);
    }
    
    private static void startPredict(String symbol, int epochs, PriceCategory category) throws Exception { 	
    	System.out.println("Creating dataset interator...");
    	//PriceCategory category = PriceCategory.OPEN; //CLOSE: Predict close price
    	iterator = new StockDataSetIterator(fileName, symbol, batchSize, exampleLength, splitRatio, category); 
    	System.out.println("Loading test dataset...");
    	
        List<Pair<INDArray, INDArray>> test = iterator.getTestDataSet();
        
        System.out.println("Building LSTM networks...");
        MultiLayerNetwork net = null;
              
        if(StockEnum.SQIA3.name().equals(symbol)) {
        	 net = RecurrentNetsSQIA3.createAndBuildLstmNetworks(iterator.inputColumns(), iterator.totalOutcomes());
        }else if(StockEnum.PETR4.name().equals(symbol)) {
        	net = RecurrentNetsPETR4.createAndBuildLstmNetworks(iterator.inputColumns(), iterator.totalOutcomes());
        }else if(StockEnum.VALE3.name().equals(symbol)) {
        	net = RecurrentNetsVALE3.createAndBuildLstmNetworks(iterator.inputColumns(), iterator.totalOutcomes());
        }else if(StockEnum.HGLG11.name().equals(symbol)) {
        	net = RecurrentNetsHGLG11.createAndBuildLstmNetworks(iterator.inputColumns(), iterator.totalOutcomes());
        }else if(StockEnum.VISC11.name().equals(symbol)) {
        	net = RecurrentNetsVISC11.createAndBuildLstmNetworks(iterator.inputColumns(), iterator.totalOutcomes());
        }
        
        //Initialize the user interface backend
        UIServer uiServer = UIServer.getInstance();
        
        //Configure where the network information (gradients, activations, score vs. time, etc) is to be stored
        //Then add the StatsListener to collect this information from the network, as it trains
        
        //Alternative: new FileStatsStorage(File) - See UIStorageExample 
        StatsStorage statsStorage = new InMemoryStatsStorage();
        
        //Attach the StatsStorage instance to the UI: this allows the contents of the StatsStorage to be visualized
        uiServer.attach(statsStorage);
        
        int listenerFrequency = 1;
        net.setListeners(new StatsListener(statsStorage, listenerFrequency));
        
        System.out.println("Training LSTM network...");
        for(int i = 0; i < epochs; i++) {
        	while(iterator.hasNext()) {
        		//fit model using mini-batch data
        		net.fit(iterator.next());
        	}
        	//reset iterator
        	iterator.reset();
        	//clear previous state
        	net.rnnClearPreviousState();
        }
        
        //Print the number of parameters in the network (and for each layer)
        
        Layer[] layer_before_saving = net.getLayers();
        int totalNumParams_before_saving = 0;
        for(int i=0; i < layer_before_saving.length; i++) {
        	int nParams = layer_before_saving[i].numParams();
        	System.out.println("Number of parameters in layer " + i + ": " + nParams);
        	totalNumParams_before_saving += nParams;
        }
        
        System.out.println("Total number of network parameters: " + totalNumParams_before_saving);
        
        System.out.println("Saving model...");
        File locationToSave = new File("data/StockPriceLSTM_".concat(String.valueOf(category)).concat(".zip"));
        
        // saveUpdater: i.e., the state for Momentum, RMSProp, Adagrad etc. Save this to train your network more in the future
        ModelSerializer.writeModel(net, locationToSave, true);
        
        System.out.println("Restoring model...");
        net = ModelSerializer.restoreMultiLayerNetwork(locationToSave);
        
        //print the score with every 1 iteration
        net.setListeners(new ScoreIterationListener(1));
        
        //print the number of Parameters in the network (and for each layer)
        Layer[] layers = net.getLayers();
        int totalNumParams = 0;
        for(int i=0; i < layers.length; i++) {
        	int nParams = layers[i].numParams();
        	System.out.println("Number of parameters in layer " + i + ": " + nParams);
        	totalNumParams += nParams; 
        }
        System.out.println("Total number of network parameters: " + totalNumParams);
        
        System.out.println("Evaluating...");
        
        if(category.equals(PriceCategory.ALL)) {
        	INDArray max = Nd4j.create(iterator.getMaxArray());
        	INDArray min = Nd4j.create(iterator.getMinArray());
        	predictAllCategories(net, test, max, min, symbol);
        }else {
        	double max = iterator.getMaxNum(category);
        	double min = iterator.getMinNum(category);
        	predictPriceOneAhead(net, test, max, min, category, symbol);
        }
     
        System.out.println("Done...");
        uiServer.stop();
    }
    
    /** Predict one feature of a stock one-day ahead 
     * @throws Exception */
    private static void predictPriceOneAhead(MultiLayerNetwork net, List<Pair<INDArray, INDArray>> testData, 
    															double max, double min, PriceCategory category, String symbol) throws Exception {
    	double[] predicts = new double[testData.size()];
    	double[] actuals = new double[testData.size()];
    	
    	for(int i=0; i < testData.size(); i++) {
    		predicts[i] = net.rnnTimeStep(testData.get(i).getKey()).getDouble(exampleLength - 1) * (max - min) + min;
    		actuals[i] = testData.get(i).getValue().getDouble(0);
    	}
    	
    	RegressionEvaluation eval = net.evaluateRegression(iterator);
    	System.out.println(eval.stats());
    	
    	System.out.println("Printing predicted and actual values...");
    	System.out.println("Predict, Actual");
    	
    	for(int i=0; i < predicts.length; i++) {
    		System.out.println(predicts[i] + "," + actuals[i]);
    	}
    	
    	System.out.println("Plotting...");
    	PlotUtil.plot(predicts, actuals, String.valueOf(category), symbol);    	
    	
    	StockBO bo = new StockBO();
    	bo.savePredict(predicts, category, symbol);
    }
    
    
    /** Predict all the features (open, close, low, high, price and volume) of a stock one-day ahead 
     * @throws Exception */
    private static void predictAllCategories(MultiLayerNetwork net, List<Pair<INDArray, 
    									INDArray>> testData, INDArray max, INDArray min, String symbol) throws Exception {
    	INDArray[] predicts = new INDArray[testData.size()];
    	INDArray[] actuals = new INDArray[testData.size()];
    	for(int i = 0; i < testData.size(); i++) {
    		predicts[i] = net.rnnTimeStep(testData.get(i).getKey())
    				.getRow(exampleLength - 1).mul(max.sub(min)).add(min);
    		actuals[i] = testData.get(i).getValue();
    	}
    	System.out.println("Printing predicted and actual values...");
    	System.out.println("Predict, Actual");
    	
    	for(int i = 0; i < predicts.length; i++) {
    		System.out.println(predicts[i] + "\t" + actuals[i]);
    	}
    	
    	System.out.println("Plotting...");
    	RegressionEvaluation eval = net.evaluateRegression(iterator);
    	System.out.println(eval.stats());
    	
    	for(int n=0; n < 5; n++) {
    		double[] pred = new double[predicts.length];
    		double[] actu = new double[actuals.length];

    		for(int i = 0; i < predicts.length; i++) {
    			pred[i] = predicts[i].getDouble(n);
    			actu[i] = actuals[i].getDouble(n);
    		}
    		PriceCategory category;
    		String name;
    		switch(n) {
	    		case 0: {
	    			name = "Stock OPEN Price";
	    			category = PriceCategory.OPEN;
	    			break;
	    		}
	    		case 1: {
	    			name = "Stock CLOSE Price";
	    			category = PriceCategory.CLOSE;
	    			break;
	    		}
	    		case 2: {
	    			name = "Stock LOW Price";
	    			category = PriceCategory.LOW;
	    			break;
	    		}
	    		case 3: {
	    			name = "Stock HIGH Price";
	    			category = PriceCategory.HIGH;
	    			break;
	    		}
	    		case 4: {
	    			name = "Stock VOLUME Price";
	    			category = PriceCategory.VOLUME;
	    			break;
	    		}
	    		default: {
	    			throw new NoSuchElementException();
	    		}
    		}
    		PlotUtil.plot(pred, actu, name, symbol);
    		
        	StockBO bo = new StockBO();
        	bo.savePredict(pred, category, symbol);
    	}
    }

}
