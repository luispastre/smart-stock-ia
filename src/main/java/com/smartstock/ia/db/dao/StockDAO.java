package com.smartstock.ia.db.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.smartstock.ia.db.config.DatabaseConnect;
import com.smartstock.ia.entity.StockEntity;

public class StockDAO {


	public void save(StockEntity p) {

		Transaction tx = null; // permite transacao com o BD

		// obtem uma sessao
		try(Session session = DatabaseConnect.getInstance()){
			tx = session.beginTransaction();
			session.saveOrUpdate(p);
			tx.commit();// faz a transacao
		} catch (Exception e) {
			e.printStackTrace();
			// cancela a transcao em caso de falha
			tx.rollback();
		} 
	}
	
	
	
	public StockEntity findBySymbol(String symbol) {

		try(Session session = DatabaseConnect.getInstance()){
			
			StringBuilder sqlQuery = new StringBuilder("");
			sqlQuery.append(" FROM StockEntity sk 								");
			sqlQuery.append("		LEFT JOIN FETCH sk.listPredictedPrice pp	");
			sqlQuery.append("	  WHERE sk.symbol = :symbol						");
			
			Query query = session.createQuery(sqlQuery.toString());
			query.setParameter("symbol", symbol);
			
			List<StockEntity> result = query.getResultList();
			
			if(!result.isEmpty()) {
				return result.get(0);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			// cancela a transcao em caso de falha
		} 
		return null;
	}
}
