package com.smartstock.ia.db.bo;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;

import com.smartstock.ia.db.dao.StockDAO;
import com.smartstock.ia.entity.StockEntity;
import com.smartstock.ia.entity.StockPredictedPriceEntity;
import com.smartstock.ia.representation.PriceCategory;

public class StockBO {

	
	public void savePredict(double[] predicts, 
							PriceCategory category, String symbol) throws Exception {
		
		StockDAO dao = new StockDAO();
		Double predictValue = predicts[predicts.length-1];
		
		StockEntity stock = dao.findBySymbol(symbol);
		
		if(stock.getListPredictedPrice().isEmpty()) {
			stock.setListPredictedPrice(new ArrayList<>());
		}

		
		Calendar tomorrow = Calendar.getInstance();
		tomorrow.add(Calendar.DATE, -6);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date dtTomorrow = sdf.parse(sdf.format(tomorrow.getTime()));
		
		
		
		Optional<StockPredictedPriceEntity> option = stock.getListPredictedPrice()
															.stream().filter(p -> {
																if(p.getIncludeDate().equals(dtTomorrow))
																	return true;
																return false;
																}).findFirst();
		
		StockPredictedPriceEntity stockPredictedPrice = new StockPredictedPriceEntity();
		if(option.isPresent()) {
			stockPredictedPrice = option.get();
		}else {
			stockPredictedPrice.setStock(stock);
			stock.getListPredictedPrice().add(stockPredictedPrice);
		}
		
		stockPredictedPrice.setIncludeDate(tomorrow.getTime());
		
		NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
		DecimalFormat df = (DecimalFormat)nf;
		df.applyPattern("##0.00");
		      
		switch(category) {
			case OPEN:
				stockPredictedPrice.setOpenPrice(Double.valueOf(df.format(predictValue)));
				break;
			case HIGH:
				stockPredictedPrice.setHighPrice(Double.valueOf(df.format(predictValue)));
				break;
			case LOW:
				stockPredictedPrice.setLowPrice(Double.valueOf(df.format(predictValue)));
				break;
			case CLOSE:
				stockPredictedPrice.setClosePrice(Double.valueOf(df.format(predictValue)));
				break;
			case VOLUME:
				stockPredictedPrice.setVolumeTmp(predictValue);
				break;
			default:
				break;
		}

		dao.save(stock);
	}
	
}
